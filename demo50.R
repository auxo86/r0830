rm(list = ls())
install.packages("ggvis")
library(ggvis)
mtcars
?mtcars
plot <- ggvis(mtcars, x=~cyl, y=~mpg)
layer_points(plot)
layer_points(ggvis(mtcars, x=~cyl, y=~mpg))
mtcars %>% ggvis(x=~cyl, y=~mpg) %>% layer_points()
