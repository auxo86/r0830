rm(list = ls())
plot(sin, 0, 4 * pi)
plot(cos, 0, pi)
plot(abs, -5, 5)
sigmoid <- function(x){
  1/(1+exp(-x))
}
plot(sigmoid, -10, 10)

relu <- function(x){
  # ifelse(x>0, x, 0)
  ifelse(x<0, 0, x)
}

plot(relu, -20, 20)
