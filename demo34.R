rm(list = ls())
func61 <- function(x) {
  3 * sin(x / 2) + x
}
result <- c(func61(0), func61(-1), func61(pi))
result

