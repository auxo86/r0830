rm(list = ls())
a1 <- list('Mark', 80, 168)
names(a1) <- c('name', 'weight', 'height')
a1
a2 <- list(name='James', weight=85, height=180, instructor = a1) # equal to a2$instructor = a1
a2
str(a1)
str(a2)
summary(a1)

# a2$instructor = a1

a1
a2


